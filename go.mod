module gitlab.com/prep-microservice-workshop/utils-go.git

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	google.golang.org/grpc v1.42.0
)
